/*
 * Pascal.java
 *
 * Author: Siddharth Bapat
 *
 */

class Solution {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ans=new ArrayList<>();
        List<Integer> temp=new ArrayList<>();
        int it=1;
        while(it<=numRows){
            temp=new ArrayList<>();
            if(it==1){
                temp.add(1);
                ans.add(temp);
            }else if(it==2){
                temp.add(1);
                temp.add(1);
                ans.add(temp);
            }else{
                List<Integer> prev=ans.get(ans.size()-1);
                temp.add(1);
                for(int i=0;i<prev.size()-1;i++){
                    temp.add(prev.get(i)+prev.get(i+1));
                }
                temp.add(1);
                ans.add(temp);
            }
            it++;
        }
        
        return ans;
    }
}
